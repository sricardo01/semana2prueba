﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConexionSql
{
    class Program
    {
        static void Main(string[] args)
        {

            String cadenaConexion = @"Server=UIOCSE-SQU\SQLEXPRESS;Database=BaseDeDatosSql;User ID=desarrolador;Password=Matias3020_";

            try
            {


                //1.  Conectar
                SqlConnection con = new SqlConnection(cadenaConexion);


                /* List<string> list = new List<string>();
                 String instruccionInser = "INSERT INTO producto(id,nombre) values (1, 'Producto 1')"            ;
                 String instruccionInser1 = "INSERT INTO producto(id,nombre) values (2, 'Producto 2')";
                 String instruccionInser2 = "INSERT INTO producto(id,nombre) values (3, 'Producto 3')";
                 String instruccionInser3 = "INSERT INTO producto(id,nombre) values (4, 'Producto 4')";
                 list.Add(instruccionInser);
                 list.Add(instruccionInser1);
                 list.Add(instruccionInser2);
                 list.Add(instruccionInser3);
                 // 3 hacer algo con la base
                 ingresarDatosSql(list, con);*/

                // Actualizar
                /*String instruccionActualizar = "UPDATE producto set nombre = 'Producto 1 Actualizado' where id = 1";
                actualizarDatosSql(instruccionActualizar, con);*/

                // Select
                /*string instruccionSelect = "Select * from producto";
                seleccionTodosLosRegistro(instruccionSelect, "nombre", con);
                seleccionTodosLosRegistro(instruccionSelect, "id", con);*/

                string instruccionElimnar = "DELETE FROM producto where id = 3";
                eliminarRegistro(instruccionElimnar, con);

                Console.ReadKey();

              


            }
            catch (Exception e) {

                Console.WriteLine("Existio un error en el sistema: " + e );
                Console.ReadKey();

            }


            // Insertar --> CREATE
            void ingresarDatosSql(List<String> listaIntrucciones, SqlConnection con)
            {
                con.Open();
                foreach (String instruccion in listaIntrucciones)
                {
                    SqlCommand comando3 = new SqlCommand(instruccion, con);
                    comando3.ExecuteNonQuery();
                    Console.WriteLine("Inserto la instruccion: " + instruccion);
                }
                con.Close();
            }


            // Update o Actualizar 
            void actualizarDatosSql(string instruccion, SqlConnection conn) {
                conn.Open();
                SqlCommand comando3 = new SqlCommand(instruccion, conn);
               comando3.ExecuteNonQuery();
                Console.WriteLine("Se actulizaco el registro con la instruccion: " + instruccion);
                conn.Close();
            }


            // Selact --> Read
            void seleccionTodosLosRegistro(String instruccion, String columna, SqlConnection conn) {
                conn.Open();
                SqlCommand comando3 = new SqlCommand(instruccion, conn);
                SqlDataReader registro = comando3.ExecuteReader();

                while(registro.Read())
                {
                    Console.WriteLine("La columna  " + columna + " tiene un valor de: " + registro[columna].ToString());

                }
                conn.Close();
            }


            void eliminarRegistro( String instruccion, SqlConnection conn)
            {
                conn.Open();
                SqlCommand comando3 = new SqlCommand(instruccion, conn);
                comando3.ExecuteNonQuery();
                Console.WriteLine("El Registro eliminado  es: " + instruccion);
                conn.Close();
            }

        }

        
    }
}
