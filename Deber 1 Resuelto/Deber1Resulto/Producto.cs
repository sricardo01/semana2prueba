﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deber1Resulto
{
    class Producto
    {
        /*
         --> Nombre --> C
	--> Proteina --> duoble --> C
	-> Vitaminas --> duoble --> C
	--> Fecha de registro --> C
         
         */




        // Atributos
        private string nombre;
        private double proteina;
        private double vitamina;
        private DateTime fechaRegistro;


        // Metodos

        /*
         
1. se debe crear un metodo llamado obtenerDescripcionProducto el cual presente  lo siguiente
"La carne de RES tiene 50 g de proteina y 70 de vitaminas".
RES --> nombreProducot
        50 --> proteina
        70 --> vitaminas
         */



 
        public virtual String obtenerDescripcionProducto()
        {
            return "El/La " + nombre + "tiene " + proteina + " g proteina y " + vitamina + " g vitamina";  
        }

        public virtual String devolverNombre()
        {
            return "El nombre del producto es: " + nombre;
        }
        // Gets and Sets


        public String Nombre {

            get { return nombre; }
            set { nombre = value; }
        }


        public double Proteina
        {

            get { return proteina; }
            set { proteina = value; }
        }

        public double Vitamina
        {

            get { return vitamina; }
            set { vitamina = value; }
        }

        public DateTime FechaRegistro {
            get { return fechaRegistro; }
            set { fechaRegistro = value; }
        }

    }
}
